# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

rails_version = 5.1.6
ruby_version = 2.4.6 (winx64)

Its better to have the above versions in your system while checking out the applcation in your local env

steps to launch the app:
	1. install the above mentioned version of ruby
		for win, download the version specific ruby insatller
		for mac, use the curl command
	2. open the ruby terminal and install the rails gem using the command "gem install rails -v 5.1.6"
	3. run the command "bundle" / "bundle install"
	4. if there are any discrepancies with the gems' version, look for the steps provided in the terminal to resolve it 
	5. Finally, enter "rails s" / "rails server" to launch the app
	6. visit "localhost:3000" in your browser to open the app

