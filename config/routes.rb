Rails.application.routes.draw do
  get 'users/new'

  get '/signup', to: 'users#new'
  post '/signup', to: 'users#create'
  get '/about', to: 'static_pages#about'
  get '/help', to: 'static_pages#help' 
  get '/contact', to:'static_pages#contact'
  root 'static_pages#home'
  resources :users
end
