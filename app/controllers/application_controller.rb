class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def root_page
  	render html: "hello world"
  end

end
