require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @user = User.new(name: "rohit", email: "rohit28@gmail.com", password: "foobar", password_confirmation: "foobar")
  end

  test "should be valid" do
    assert @user.valid?
  end

  test "without name" do
    @user.name = "  "
    assert_not @user.valid?
  end

  test "without email" do
    @user.email = "  "
    assert_not @user.valid?
  end

  test "name should not be too long" do
    @user.name = "a"*51
    assert_not @user.valid?
  end

  test "email should not be too long" do
    @user.name = "a"*256 + "@example.com"
    assert_not @user.valid?
  end

  test "email address should be a valid address" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                          first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |address|
      @user.email = address
      assert @user.valid?, "#{address.inspect} should be valid"
    end
  end

  test "reject invalid email address" do
    invalid_email_addresses = %w[user@example,com user_at_foo.org user.name@example.
                                  foo@bar_baz.com foo@bar+baz.com foo@bar..com]
    invalid_email_addresses.each do |invalid_email|
      @user.email = invalid_email
      assert_not @user.valid?, "#{invalid_email} should be invalid"
    end
  end

  test "email uniqueness" do
    duplicate_user = @user.dup
    @user.save
    assert_not duplicate_user.valid?
  end

  test "verify that email is saved in lowercase" do
    jargon_mail = "TwqED@gmail.com"
    @user.email = jargon_mail
    @user.save
    assert_equal jargon_mail.downcase, @user.reload.email
  end

  test "verify that password field is not blank" do
    @user.password = @user.password_confirmation = " "*6
    @user.save
    assert_not @user.valid?
  end

  test "check pwd meets length requirement" do
    @user.password_confirmation = @user.password = "a"*5
    assert_not @user.valid?
  end
end
