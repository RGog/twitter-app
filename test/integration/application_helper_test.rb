require 'test_helper'

class ApplicationHelperTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  test "page title" do
    get help_path
    assert_select "title", full_title("Help")
    get about_path
    assert_select "title", full_title("About")
    get contact_path
    assert_select "title", full_title("Contact")
    get signup_path
    assert_select "title", full_title("Sign up")
  end
end
